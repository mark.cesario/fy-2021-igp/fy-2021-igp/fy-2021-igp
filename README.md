## Mark Cesario - FY 21 Indidivual Growth Plan (IGP)

### Objectives

### Key Performance Indicators (KPI)

KPIs for FY 21 will map to the "Carrer Plan Focus Area" below.

| Focus Area |Activity | KPI | Status |
| ----------- | ------ | ------ | ----- |
| Leadership, Management and Sales | Readings | 12 Books | 3C, 2W |
|  | Meetup / User Group | At least 2 Meetup session delivered in Denver area |1W |
| GitLab Technologoies | Deep Dive Capability | 6 areas | |
| Certifications | AWS and GCP | AWS and GCP Certified SA | 1W  |
| Other Technolgoies | GitLab related technology learning | Google ML/AI and Business Analytics/AI | |


Note: W=In Process, C=Complted


### Carrer Plan Focus Areaa

#### Leadership

  
*  [Readings](https://gitlab.com/groups/mark.cesario/fy-2021-igp/fy-2021-igp/-/epics/7)
     -  Management / Leadership / Sales
        <br/>
    - Technical
    <br/>
* [Meetup / User Group](https://gitlab.com/mark.cesario/fy-2021-igp/fy-2021-igp/fy-2021-igp/-/issues/11)

    *  Denver area GitLab Meetup User Group
    *  Manage/deliver 2 Meetup presentations 

        
#### [GitLab Technologies](https://gitlab.com/groups/mark.cesario/fy-2021-igp/fy-2021-igp/-/epics/3)

*   Kubernetes/Helm Charts
*   Project & Portfolio Management
*   Security
*   Serveless
*   Integrations (Jira / Jenkins)
*   ML/AI

#### [Certifications](https://gitlab.com/groups/mark.cesario/fy-2021-igp/fy-2021-igp/-/epics/2)

* AWS Solutions Architect
* Google Professional Cloud]
   

#### [Other Technologies](https://gitlab.com/groups/mark.cesario/fy-2021-igp/fy-2021-igp/-/epics/4)

*   Anthos/Istio/Knative/Kubeflow
*   Business Analytics and AI
    *  Stream Analytics
    *  Tensorflow
    *  Warehousing - Big Query

## Workflow Summary

Following is the defined workflow for the FY21 IGP process.

| Stage (Label) | Completion Criteria | Scoped Label |
| ------ | ------ | -------|
| IGP21::Planned | Enter issue details, Due Date and Milestone| ![planned](/image/planned.png) |
| IGP21::In Process | Working on the issue | ![inprocess](/image/inprocess.png) | 
| IGP21::Completed | Complete issue comments, lessons learned, etc. | ![completed](/image/completed.png) |

